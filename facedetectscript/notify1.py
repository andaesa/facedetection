import numpy as np
import cv2
import glob, requests

class FaceDetect():
	def detect1():
		face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')

		for img in glob.glob('/Users/Nasarudin/Pythonfiles/Faces/*.jpg'):
			cv_img = cv2.imread(img)
			gray = cv2.cvtColor(cv_img, cv2.COLOR_BGR2GRAY)
			faces1 = face_cascade.detectMultiScale(gray, 1.3, 5)
		
			for (x,y,w,h) in faces1:
				cv2.rectangle(cv_img,(x,y),(x+w,y+h),(255,0,0),2)


	def detect2():
		face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')

		for image in glob.glob('/Users/Nasarudin/Pythonfiles/testfolder/*.jpg'):
			cv_image = cv2.imread(image)
			gray = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
			faces2 = face_cascade.detectMultiScale(gray, 1.3, 5)

			for (x,y,w,h) in faces2:
				cv2.rectangle(cv_image,(x,y),(x+w,y+h),(255,0,0),2)

	def notify():
		key = "key-4a480b5dcb7a1c44e4b4d1341be27d3a"
		sandbox = "sandboxf35ebda54a494a83860938ce69655.mailgun.org"
		recipient = "nasarudinabdulshukor@gmail.com"

		request_url = 'https://api.mailgun.net/v2/{0}/messages'.format(sandbox)
		request = requests.post(request_url, auth=('api', key),
			data={
			'from': 'hello@example.com',
			'to': recipient,
			'subject': 'face detect',
			'text': 'common face detected'
		})
		print 'Status: {0}'.format(request.status_code)
		print 'Body:   {0}'.format(request.text)

	if detect1 == detect2:
		face_detect = FaceDetect()
		face_detect.run()
