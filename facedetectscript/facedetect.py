import numpy as np
import cv2


#https://github.com/opencv/opencv/tree/master/data/haarcascades
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')

#open the webcam
camera = cv2.VideoCapture(0)

#for detecting faces in an image, remove the 'while True: ret, image = camera.read'
#and add 'image = cv2.imread('picture.jpg')'
while True:
	ret, image = camera.read()
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    
    #draw rectangle to detect whole face
	for (x,y,w,h) in faces:
		cv2.rectangle(image,(x,y),(x+w,y+h),(255,0,0),2)
		roi_gray = gray[y:y+h, x:x+w]
		roi_color = image[y:y+h, x:x+w]

        #draw rectangle to detect eyes
		eyes = eye_cascade.detectMultiScale(roi_gray)
		for (ex,ey,ew,eh) in eyes:
			cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)

	cv2.imshow('webcam', image)

	#close window by hitting 'escape' key
	if (cv2.waitKey(10) == 27):
		break

camera.release()
cv2.destroyAllWindows()





	
